#include "lrp.h"

bool lrp::Problem::e1vehCapacityConstraint(Solution & sol)
{
	bool satisfied = true;
	unsigned int offset = getNumSources();
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double demand = 0.0;
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			for (unsigned int l = 0; l < getNumSources(); l++)
			{
				demand += sol.fractionOfDemand[l][i][r]
					* getFacilityDemand(l, i, sol);
			}
		}
		if (std::isgreater(demand, e1Vehicles.at(E1_VEH_IDX).getMaxCapacity()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::e2vehCapacityConstraint(Solution & sol)
{
	bool satisfied = true;
	unsigned int offset = getNumFacilities();
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double sum = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
			{
				for (unsigned int m = 0; m < getNumSources(); m++)
				{
					sum += customers.at(j).getDemand(m)*sol.secondEchelonRoutes[i][j + offset][k];
				}
			}
		}
		if (std::isgreater(sum, e2Vehicles.at(k).getMaxCapacity()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::e1rteLengthConstraint(Solution & sol)
{
	bool satisfied = true;
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double sum = 0.0;
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				sum += e1Distances.at_element(l, i)*sol.firstEchelonRoutes[l][i][r];
			}
		}
		if (std::isgreater(sum, e1Vehicles.at(E1_VEH_IDX).getMaxTravelDistance()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::e2rteLengthConstraint(Solution & sol)
{
	bool satisfied = true;
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double sum = 0.0;
		for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
		{
			for (unsigned int j = 0; j < getNumCustomers() + getNumFacilities(); j++)
			{
				sum += e2Distances.at_element(i, j)*sol.secondEchelonRoutes[i][j][k];
			}
		}
		if (std::isgreater(sum, e2Vehicles.at(k).getMaxTravelDistance()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::e1rteDurationConstraint(Solution & sol)
{
	bool satisfied = true;
	unsigned int offset = getNumSources();
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double unloadTime = 0.0;
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
			{
				unloadTime += e1UnloadTimes.at_element(E1_VEH_IDX, i)*sol.firstEchelonRoutes[l][i + offset][r];
			}
		}
		double travelTime = 0.0;
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				travelTime += e1TravelTimes[l][i][E1_VEH_IDX] * sol.firstEchelonRoutes[l][i][r];
			}
		}
		if (std::isgreater(unloadTime + travelTime, e1Vehicles.at(E1_VEH_IDX).getMaxTravelTime()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::e2rteDurationConstraint(Solution & sol)
{
	bool satisfied = true;
	unsigned int offset = getNumFacilities();
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double unloadTime = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
			{
				unloadTime += e2UnloadTimes.at_element(k, j)*sol.secondEchelonRoutes[i][j + offset][k];
			}
		}
		double travelTime = 0.0;
		for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
		{
			for (unsigned int j = 0; j < getNumCustomers() + getNumFacilities(); j++)
			{
				travelTime += e2TravelTimes[i][j][k] * sol.secondEchelonRoutes[i][j][k];
			}
		}
		if (std::isgreater(unloadTime + travelTime, e2Vehicles.at(k).getMaxTravelTime()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

bool lrp::Problem::facCapacityConstraint(Solution & sol)
{
	bool satisfied = true;
	for (unsigned int i = 0; i < getNumFacilities(); i++)
	{
		double sum = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int m = 0; m < getNumSources(); m++)
			{
				sum += customers.at(j).getDemand(m)*sol.facilityCustomer.at_element(i, j);
			}
		}
		if (std::isgreater(sum, facilities.at(i).getMaxCapacity()))
		{
			satisfied = false;
			break;
		}
	}
	return satisfied;
}

/*
	CONSTRAINT VIOLATION
*/

double lrp::Problem::e1vehCapacityViolation(Solution & sol)
{
	double violation = 0.0;
	unsigned int offset = getNumSources();
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double demand = 0.0;
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			for (unsigned int l = 0; l < getNumSources(); l++)
			{
				demand += sol.fractionOfDemand[l][i][r]
					* getFacilityDemand(l, i, sol);
			}
		}
		if (std::isgreater(demand, e1Vehicles.at(E1_VEH_IDX).getMaxCapacity()))
			violation += demand - e1Vehicles.at(E1_VEH_IDX).getMaxCapacity();
	}
	return violation;
}

double lrp::Problem::e2vehCapacityViolation(Solution & sol)
{
	double violation = 0.0;
	unsigned int offset = getNumFacilities();
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double sum = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
			{
				for (unsigned int m = 0; m < getNumSources(); m++)
				{
					sum += customers.at(j).getDemand(m)*sol.secondEchelonRoutes[i][j + offset][k];
				}
			}
		}
		if (std::isgreater(sum, e2Vehicles.at(k).getMaxCapacity()))
			violation += sum - e2Vehicles.at(k).getMaxCapacity();
	}
	return violation;
}

double lrp::Problem::e1rteLengthViolation(Solution & sol)
{
	double violation = 0.0;
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double sum = 0.0;
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				sum += e1Distances.at_element(l, i)*sol.firstEchelonRoutes[l][i][r];
			}
		}
		if (std::isgreater(sum, e1Vehicles.at(E1_VEH_IDX).getMaxTravelDistance()))
			violation += sum - e1Vehicles.at(E1_VEH_IDX).getMaxTravelDistance();
	}
	return violation;
}

double lrp::Problem::e2rteLengthVioliation(Solution & sol)
{
	double violation = 0.0;
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double sum = 0.0;
		for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
		{
			for (unsigned int j = 0; j < getNumCustomers() + getNumFacilities(); j++)
			{
				sum += e2Distances.at_element(i, j)*sol.secondEchelonRoutes[i][j][k];
			}
		}
		if (std::isgreater(sum, e2Vehicles.at(k).getMaxTravelDistance()))
			violation += sum - e2Vehicles.at(k).getMaxTravelDistance();
	}
	return violation;
}

double lrp::Problem::e1rteDurationVioliation(Solution & sol)
{
	double violation = 0.0;
	unsigned int offset = getNumSources();
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		double unloadTime = 0.0;
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
			{
				unloadTime += e1UnloadTimes.at_element(E1_VEH_IDX, i)*sol.firstEchelonRoutes[l][i + offset][r];
			}
		}
		double travelTime = 0.0;
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				travelTime += e1TravelTimes[l][i][E1_VEH_IDX] * sol.firstEchelonRoutes[l][i][r];
			}
		}
		if (std::isgreater(unloadTime + travelTime, e1Vehicles.at(E1_VEH_IDX).getMaxTravelTime()))
			violation += unloadTime + travelTime - e1Vehicles.at(E1_VEH_IDX).getMaxTravelTime();
	}
	return violation;
}

double lrp::Problem::e2rteDurationVioliation(Solution & sol)
{
	double violation = 0.0;
	unsigned int offset = getNumFacilities();
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		double unloadTime = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
			{
				unloadTime += e2UnloadTimes.at_element(k, j)*sol.secondEchelonRoutes[i][j + offset][k];
			}
		}
		double travelTime = 0.0;
		for (unsigned int i = 0; i < getNumCustomers() + getNumFacilities(); i++)
		{
			for (unsigned int j = 0; j < getNumCustomers() + getNumFacilities(); j++)
			{
				travelTime += e2TravelTimes[i][j][k] * sol.secondEchelonRoutes[i][j][k];
			}
		}
		if (std::isgreater(unloadTime + travelTime, e2Vehicles.at(k).getMaxTravelTime()))
			violation += unloadTime + travelTime - e2Vehicles.at(k).getMaxTravelTime();
	}
	return violation;
}

double lrp::Problem::facCapacityVioliation(Solution & sol)
{
	double violation = 0.0;
	for (unsigned int i = 0; i < getNumFacilities(); i++)
	{
		double sum = 0.0;
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int m = 0; m < getNumSources(); m++)
			{
				sum += customers.at(j).getDemand(m)*sol.facilityCustomer.at_element(i, j);
			}
		}
		if (std::isgreater(sum, facilities.at(i).getMaxCapacity()))
			violation += sum - facilities.at(i).getMaxCapacity();
	}
	return violation;
}