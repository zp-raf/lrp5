#pragma once

#include <fstream>
#include <string>

class Log {
public:
	enum class Level {
		Debug,
		Information,
		Notice,
		Warning,
		Error
	};
	Log(const char * logName);
	Log();
	~Log() { logFile.close(); };	
	const char * getName() { return this->name.c_str(); };
	void message(const std::string msg, const char pad, unsigned int pad_qty, bool printTime = true, Level lvl = Level::Information);
	void message(const std::string msg);
protected:
	std::ofstream logFile;
	void printTime();
private:
	std::string name;
	void processLevel(Level);
};