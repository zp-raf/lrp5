#include "log.h"
#include <ctime>
#include <iomanip>
#include <sstream>


void Log::printTime()
{
	struct tm timeinfo;
	time_t t = std::time(nullptr);
	localtime_s(&timeinfo, &t);
	logFile << "[" << std::put_time(&timeinfo, "%d-%m-%Y/%H:%M:%S") << "]";
}

void Log::processLevel(Level lvl)
{
	switch (lvl)
	{
	case Log::Level::Debug:
	{
		logFile << "[DEBUG]\t";
		break;
	}
	case Log::Level::Information:
	{
		logFile << "[INFO]  \t";
		break;
	}
	case Log::Level::Notice:
	{
		logFile << "[NOTICE]\t";
		break;
	}
	case Log::Level::Warning:
	{
		logFile << "[WARNING]\t";
		break;
	}
	case Log::Level::Error:
	{
		logFile << "[ERROR]\t";
		break;
	}
	default:
		break;
	}
}

Log::Log(const char * name)
{
	std::ostringstream logName;
	struct tm timeinfo;
	time_t t = std::time(nullptr);
	localtime_s(&timeinfo, &t);
	logName << name << "_" << std::put_time(&timeinfo, "%d%m%Y-%H%M%S"); 
	this->name = logName.str();
	logName << ".log";
	logFile.open(logName.str());
}

Log::Log()
{
	std::ostringstream logName;
	struct tm timeinfo;
	time_t t = std::time(nullptr);
	localtime_s(&timeinfo, &t);
	logName << std::put_time(&timeinfo, "%d%m%Y-%H%M%S"); 
	this->name = logName.str();
	logName<< ".log";
	logFile.open(logName.str());
}

void Log::message(const std::string msg, const char pad, unsigned int pad_qty, bool printTime, Log::Level lvl)
{
	if (printTime)
		this->printTime();
	processLevel(lvl);
	for (unsigned int i = 0; i < pad_qty * 2; i++)
	{
		if (i == pad_qty)
			logFile << " " << msg << " ";
		else
			logFile << pad;
	}
	if (pad_qty == 0)
		logFile << msg;
	logFile << std::endl;
}

void Log::message(const std::string msg)
{
	message(msg, ' ', 0);
}
