#include <exception>
#include <math.h>
#include "mobjective.h"
#include "lrp.h"

using namespace mobjective;

bool mobjective::dominates(BaseSolution & subject, BaseSolution & test)
{
	if (subject.objectives.size() != subject.objectives.size())
		throw "Number of objectives do not agree";

	unsigned int strictlyBetter = 0;
	unsigned int worse = 0;
	for (unsigned int i = 0; i < subject.objectives.size(); ++i)
	{
		if (subject.objectives.at(i).value < test.objectives.at(i).value)
			++strictlyBetter;
		else if (subject.objectives.at(i).value > test.objectives.at(i).value)
			++worse;
	}

	if (worse == 0 && strictlyBetter >= 1)
		return true;
	else
		return false;
}

bool mobjective::equals(BaseSolution & subject, BaseSolution & test)
{
	if (subject.objectives.size() != subject.objectives.size())
		throw "Number of objectives do not agree";
	for (unsigned int i = 0; i < subject.objectives.size(); ++i)
	{
		if (subject.objectives.at(i).value < test.objectives.at(i).value
			|| subject.objectives.at(i).value > test.objectives.at(i).value)
			return false;
	}
	return true;
}