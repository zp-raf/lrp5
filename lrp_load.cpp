#include "lrp.h"
#include <cassert>
#include "loader.cpp"

void lrp::Problem::fetchData(std::string filename)
{
	pt::ptree treeRoot;

	// Load the json file
	pt::read_json(filename, treeRoot);

	// Load sources
	lrp::loader::vectorLoader<Source>("sources", treeRoot, sources, [](pt::ptree tree) {
		return Source(tree.get<std::string>("name"), tree.get<double>("costPerDeliveryUnit"));
	});

	// Load facilities
	lrp::loader::vectorLoader<Facility>("facilities", treeRoot, facilities, [](pt::ptree tree) {
		return Facility(tree.get<double>("openingCost"),
			tree.get<double>("costPerDeliveryUnit"),
			tree.get<double>("maxCapacity"),
			tree.get<std::string>("name"));
	});

	// Load customers
	lrp::loader::vectorLoader<Customer>("customers", treeRoot, customers, [](pt::ptree tree) {
		std::vector<double> tempDemand;
		for (auto &c : tree.get_child("demand"))
		{
			tempDemand.push_back(c.second.get_value<double>(NULL));
		}
		return Customer(tempDemand, tree.get<std::string>("name"));
	});

	// Load pollutants
	lrp::loader::vectorLoader<Pollutant>("pollutants", treeRoot, pollutants, [](pt::ptree tree) {
		return Pollutant(tree.get<std::string>("name"));
	});

	// Load first-echelon vehicle data
	lrp::loader::vectorLoader<Vehicle>("e1vehicles", treeRoot, e1Vehicles, [](pt::ptree tree) {
		return Vehicle(tree.get<double>("maxCapacity"),
			tree.get<double>("maxTravelDistance"),
			tree.get<double>("maxTravelTime"),
			tree.get<double>("costPerDistance"),
			tree.get<std::string>("name"));
	});

	// Load first-echelon emission factor matrix
	lrp::loader::matrixLoader<double>("e1emissionFactors", treeRoot, getE1NumVehicles(), getNumPollutants(), e1EmissionFactors, [](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load second-echelon vehicle data
	lrp::loader::vectorLoader<Vehicle>("e2vehicles", treeRoot, e2Vehicles, [](pt::ptree tree) {
		return Vehicle(tree.get<double>("maxCapacity"),
			tree.get<double>("maxTravelDistance"),
			tree.get<double>("maxTravelTime"),
			tree.get<double>("costPerDistance"),
			tree.get<std::string>("name"));
	});

	// Load second-echelon emission factor matrix
	lrp::loader::matrixLoader<double>("e2emissionFactors", treeRoot, getE2NumVehicles(), getNumPollutants(), e2EmissionFactors, [](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load maximum number of first-echelon routes
	e1MaxNumRoutes = treeRoot.get<unsigned int>("e1maxNumRoutes");
	if (e1MaxNumRoutes <= 0)
		throw std::exception("Maximum number of routes must be greater than zero");
	else if (e1MaxNumRoutes < getNumSources())
		throw std::exception("Maximum number of routes must be greater or equal than the number of sources");

	if (getE1MaxNumRoutes() < getNumSources())
		throw std::exception("Number of E1 vehicles must not be inferior to number of sources");
	if (getE2NumVehicles() < getNumFacilities())
		throw std::exception("Number of E2 vehicles must not be inferior to number of facilities");

	// Load first-echelon distance matrix
	lrp::loader::matrixLoader<double>("e1distances", treeRoot, sources.size() + facilities.size(), sources.size() + facilities.size(), e1Distances,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load second-echelon distance matrix
	lrp::loader::matrixLoader<double>("e2distances", treeRoot, facilities.size() + customers.size(), facilities.size() + customers.size(), e2Distances,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load first-echelon fixed delivery cost matrix
	lrp::loader::matrixLoader<double>("e1fixedCosts", treeRoot, sources.size(), facilities.size(), e1FixedCosts,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load second-echelon fixed delivery cost matrix
	lrp::loader::matrixLoader<double>("e2fixedCosts", treeRoot, facilities.size(), customers.size(), e2FixedCosts,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load first-echelon unloading time matrix
	lrp::loader::matrixLoader<double>("e1unloadTimes", treeRoot, e1Vehicles.size(), facilities.size(), e1UnloadTimes,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load first-echelon unloading time matrix
	lrp::loader::matrixLoader<double>("e2unloadTimes", treeRoot, e2Vehicles.size(), customers.size(), e2UnloadTimes,
		[](pt::ptree tree) {
		return tree.get_value<double>(NULL);
	});

	// Load first-echelon travel times array
	unsigned int tempSz = sources.size() + facilities.size();
	Problem::TravelTimesArray::extent_gen extents;
	e1TravelTimes.resize(extents[tempSz][tempSz][getE1NumVehicles()]);
	unsigned int index_k = 0;
	for (auto k : treeRoot.get_child("e1travelTimes"))
	{
		unsigned int index_i = 0;
		for (auto i : k.second)
		{
			unsigned int index_j = 0;
			for (auto j : i.second)
			{
				auto tempVal = j.second.get_value<double>(NULL);
				e1TravelTimes[index_i][index_j][index_k] = tempVal;
				//assert(target.travelTimes[index_i][index_j][index_k] == tempVal);
				index_j++;
			}
			if (index_j != tempSz)
				throw std::exception("Loading error: Invalid size for first dimension of the E1 travel times array");
			index_i++;
		}
		if (index_i != tempSz)
			throw std::exception("Loading error: Invalid size for second dimension of the E1 travel times array");
		index_k++;
	}
	if (index_k != e1Vehicles.size())
		throw std::exception("Loading error: Invalid size for third dimension of the E1 travel times array");

	// Load second-echelon travel times array
	tempSz = facilities.size() + customers.size();
	e2TravelTimes.resize(extents[tempSz][tempSz][getE2NumVehicles()]);
	index_k = 0;
	for (auto k : treeRoot.get_child("e2travelTimes"))
	{
		unsigned int index_i = 0;
		for (auto i : k.second)
		{
			unsigned int index_j = 0;
			for (auto j : i.second)
			{
				auto tempVal = j.second.get_value<double>(NULL);
				e2TravelTimes[index_i][index_j][index_k] = tempVal;
				//assert(target.travelTimes[index_i][index_j][index_k] == tempVal);
				index_j++;
			}
			if (index_j != tempSz)
				throw std::exception("Loading error: Invalid size for first dimension of the E2 travel times array");
			index_i++;
		}
		if (index_i != tempSz)
			throw std::exception("Loading error: Invalid size for second dimension of the E2 travel times array");
		index_k++;
	}
	if (index_k != e2Vehicles.size())
		throw std::exception("Loading error: Invalid size for third dimension of the E2 travel times array");
}

lrp::Solution lrp::loader::getLrpSolution(std::string filename, Problem & targetProblem)
{
	pt::ptree root;
	pt::read_json(filename, root);

	unsigned int numSrc = targetProblem.getNumSources();
	unsigned int numFac = targetProblem.getNumFacilities();
	unsigned int numCust = targetProblem.getNumCustomers();
	unsigned int numE1Veh = targetProblem.getE1MaxNumRoutes();
	unsigned int numE2Veh = targetProblem.getE2NumVehicles();

	Solution sol(targetProblem);

	/*
	Load first-echelon routes vector
	*/

	Solution::RouteArray::extent_gen extents;
	unsigned int size_i = numSrc + numFac;
	unsigned int size_j = size_i;
	unsigned int size_k = numE1Veh;
	sol.firstEchelonRoutes.resize(extents[size_i][size_j][size_k]);
	unsigned int index_k = 0;
	for (auto k : root.get_child("e1routes"))
	{
		unsigned int index_i = 0;
		for (auto i : k.second)
		{
			unsigned int index_j = 0;
			for (auto j : i.second)
			{
				auto tempVal = j.second.get_value<unsigned int>(NULL);
				sol.firstEchelonRoutes[index_i][index_j][index_k] = tempVal;
				//assert(target.travelTimes[index_i][index_j][index_k] == tempVal);
				index_j++;
			}
			if (index_j != size_j)
				throw std::exception("Loading error: Invalid size for first dimension of the E1 routes array");
			index_i++;
		}
		if (index_i != size_i)
			throw std::exception("Loading error: Invalid size for second dimension of the E1 routes array");
		index_k++;
	}
	if (index_k != size_k)
		throw std::exception("Loading error: Invalid size for third dimension of the E1 routes array");

	/*
	Load second-echelon routes vector
	*/

	size_i = numFac + numCust;
	size_j = size_i;
	size_k = numE2Veh;
	sol.secondEchelonRoutes.resize(extents[size_i][size_j][size_k]);
	index_k = 0;
	for (auto k : root.get_child("e2routes"))
	{
		unsigned int index_i = 0;
		for (auto i : k.second)
		{
			unsigned int index_j = 0;
			for (auto j : i.second)
			{
				auto tempVal = j.second.get_value<unsigned int>(NULL);
				sol.secondEchelonRoutes[index_i][index_j][index_k] = tempVal;
				//assert(target.travelTimes[index_i][index_j][index_k] == tempVal);
				index_j++;
			}
			if (index_j != size_j)
				throw std::exception("Loading error: Invalid size for first dimension of the E2 routes array");
			index_i++;
		}
		if (index_i != size_i)
			throw std::exception("Loading error: Invalid size for second dimension of the E2 routes array");
		index_k++;
	}
	if (index_k != size_k)
		throw std::exception("Loading error: Invalid size for third dimension of the E2 routes array");


	/*
	Load facility status data
	*/

	sol.facilityStatus.resize(numFac, 0);
	unsigned int index_i = 0;
	for (auto i : root.get_child("facilityStatus"))
	{
		sol.facilityStatus.at(index_i) = i.second.get_value<unsigned int>(NULL);
		index_i++;
	}
	if (index_i != numFac)
		throw std::exception("Invalid facility status vector size");

	/*
	Load Facility-User Assignment
	*/

	sol.facilityCustomer.resize(numFac, numCust);
	index_i = 0;
	for (auto i : root.get_child("facilityUserAssignment"))
	{
		unsigned int index_j = 0;
		for (auto j : i.second)
		{
			sol.facilityCustomer.at_element(index_i, index_j) = j.second.get_value<unsigned int>(NULL);
			index_j++;
		}
		if (index_j != numCust)
			throw std::exception("Invalid facility-customer matrix column size");
		index_i++;
	}
	if (index_i != numFac)
		throw std::exception("Invalid facility-customer matrix row size");

	/*
	Load source-facitlity assignment
	*/

	sol.sourceFacility.resize(numSrc, numFac);
	index_i = 0;
	for (auto i : root.get_child("sourceFacilityAssignment"))
	{
		unsigned int index_j = 0;
		for (auto j : i.second)
		{
			sol.sourceFacility.at_element(index_i, index_j) = j.second.get_value<unsigned int>(NULL);
			index_j++;
		}
		if (index_j != numFac)
			throw std::exception("Invalid facility-customer matrix column size");
		index_i++;
	}
	if (index_i != numSrc)
		throw std::exception("Invalid facility-customer matrix row size");

	/*
	Load fraction of demand vector
	*/

	size_i = numSrc;
	size_j = numFac;
	size_k = numE1Veh;
	sol.fractionOfDemand.resize(extents[size_i][size_j][size_k]);
	index_k = 0;
	for (auto k : root.get_child("fractionDemand"))
	{
		unsigned int index_i = 0;
		for (auto i : k.second)
		{
			unsigned int index_j = 0;
			for (auto j : i.second)
			{
				auto tempVal = j.second.get_value<unsigned int>(NULL);
				sol.fractionOfDemand[index_i][index_j][index_k] = tempVal;
				//assert(target.travelTimes[index_i][index_j][index_k] == tempVal);
				index_j++;
			}
			if (index_j != size_j)
				throw std::exception("Loading error: Invalid size for first dimension of E1 fraction of demand array");
			index_i++;
		}
		if (index_i != size_i)
			throw std::exception("Loading error: Invalid size for second dimension of E1 fraction of demand array");
		index_k++;
	}
	if (index_k != size_k)
		throw std::exception("Loading error: Invalid size for third dimension of E1 fraction of demand array");

	return sol;
}