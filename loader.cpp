#include "lrp.h"

// A generic loader for vectors
template<class T>
void lrp::loader::vectorLoader(const std::string key, const pt::ptree& treeRoot, std::vector<T>& data, std::function<T(pt::ptree)> creator)
{
	data.clear();
	for (auto i : treeRoot.get_child(key))
	{
		// Load data
		data.push_back(creator(i.second));
	}
}

// A generic loader for boost matrices
template<class T>
void lrp::loader::matrixLoader(const std::string key, const pt::ptree& treeRoot, unsigned int size_i, unsigned int size_j, ublas::matrix<T>& data, std::function<T(pt::ptree)> creator)
{
	data.resize(size_i, size_j, false);
	unsigned int index_i = 0, index_j = 0;
	try
	{
		for (auto i : treeRoot.get_child(key))
		{
			index_j = 0;
			if (index_i >= size_i)
				throw std::exception("Invalid number of rows");
			for (auto j : i.second)
			{
				if (index_j >= size_j)
					throw std::exception("Invalid number of columns");
				data.at_element(index_i, index_j) = creator(j.second);
				index_j++;
			}
			if (index_j < size_j)
				throw std::exception("Invalid number of columns");
			index_i++;
		}
		if (index_i < size_i)
			throw std::exception("Invalid number of rows");
	}
	catch (const std::exception& e)
	{
		std::string msg("An error ocurred while loading matrix ");
		msg.append(key).append(". Error message: ").append(e.what());
		msg.append(". ");
		throw std::exception(msg.c_str());
	}
}