#pragma once

#include <memory>
#include <utility>
#include <vector>

namespace mobjective {
	// The optimization type (minimization or maximization)
	enum class OptType
	{
		Min,
		Max
	};

	// A data structure for storing an objective value and its respective optimization type
	//typedef double Objective;
	struct Objective {
		double value;
		OptType type;
		std::string name;
	};

	// A vector for storing objective values for a multi-objective problem
	typedef std::vector<Objective> Objectives;

	// A generic multi-objective problem solution
	struct BaseSolution {
		Objectives objectives;
		bool feasible;
	};

	bool dominates(BaseSolution & subject, BaseSolution & test);
	bool equals(BaseSolution & subject, BaseSolution & test);
}