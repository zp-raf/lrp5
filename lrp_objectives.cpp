#include "lrp.h"

double lrp::Problem::openingCosts(Solution & sol)
{
	double openingCosts = 0.0;
	for (unsigned int i = 0; i < getNumFacilities(); i++)
	{
		openingCosts += sol.facilityStatus.at(i)*facilities.at(i).getOpeningCost();
	}
	return openingCosts;
}

double lrp::Problem::e1operatingCosts(Solution & sol)
{
	double operatingCosts = 0.0;
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{		
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				operatingCosts += e1Distances.at_element(l, i)*sol.firstEchelonRoutes[l][i][r] * e1Vehicles.at(E1_VEH_IDX).getCostPerDistance();
			}
		}
	}
	return operatingCosts;
}

double lrp::Problem::e2operatingCosts(Solution & sol)
{
	double operatingCosts = 0.0;
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		for (unsigned int i = 0; i < getNumFacilities() + getNumCustomers(); i++)
		{
			for (unsigned int j = 0; j < getNumFacilities() + getNumCustomers(); j++)
			{
				operatingCosts += e2Distances.at_element(i, j)*sol.secondEchelonRoutes[i][j][k] * e2Vehicles.at(k).getCostPerDistance();
			}
		}
	}
	return operatingCosts;
}

double lrp::Problem::e1COemissions(Solution & sol)
{
	double co = 0.0;
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				co += e1Distances.at_element(l, i)*sol.firstEchelonRoutes[l][i][r] * e1EmissionFactors.at_element(E1_VEH_IDX, CO_IDX);
			}
		}
	}
	return co;
}

double lrp::Problem::e1CO2emissions(Solution & sol)
{
	double co2 = 0.0;
	for (unsigned int r = 0; r < getE1MaxNumRoutes(); r++)
	{
		for (unsigned int l = 0; l < getNumSources() + getNumFacilities(); l++)
		{
			for (unsigned int i = 0; i < getNumSources() + getNumFacilities(); i++)
			{
				co2 += e1Distances.at_element(l, i)*sol.firstEchelonRoutes[l][i][r] * e1EmissionFactors.at_element(E1_VEH_IDX, CO2_IDX);
			}
		}
	}
	return co2;
}

double lrp::Problem::e2COemissions(Solution & sol)
{
	double co = 0.0;
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		for (unsigned int i = 0; i < getNumFacilities() + getNumCustomers(); i++)
		{
			for (unsigned int j = 0; j < getNumFacilities() + getNumCustomers(); j++)
			{
				co += e2Distances.at_element(i, j)*sol.secondEchelonRoutes[i][j][k] * e2EmissionFactors.at_element(k, CO_IDX);
			}
		}
	}
	return co;
}

double lrp::Problem::e2CO2emissions(Solution & sol)
{
	double co2 = 0.0;
	for (unsigned int k = 0; k < getE2NumVehicles(); k++)
	{
		for (unsigned int i = 0; i < getNumFacilities() + getNumCustomers(); i++)
		{
			for (unsigned int j = 0; j < getNumFacilities() + getNumCustomers(); j++)
			{
				co2 += e2Distances.at_element(i, j)*sol.secondEchelonRoutes[i][j][k] * e2EmissionFactors.at_element(k, CO2_IDX);
			}
		}
	}
	return co2;
}

double lrp::Problem::e1shippingCosts(Solution & sol)
{
	double varCosts = 0.0;
	for (unsigned int l = 0; l < getNumSources(); l++)
	{
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			varCosts += getFacilityDemand(l, i, sol)*sol.sourceFacility.at_element(l, i)*sources.at(l).getVarCost();
			//// supplied demand of CDC i in I
			//double demand = 0.0;
			//for (unsigned int j = 0; j < getNumCustomers(); j++)
			//{
			//	varCosts += customers.at(j).getDemand(l)*sol.facilityCustomer.at_element(i, j)*sol.sourceFacility.at_element(l, i)*sources.at(l).getVarCost();
			//}
		}
	}

	double fixedCosts = 0.0;
	for (unsigned int l = 0; l < getNumSources(); l++)
	{
		for (unsigned int i = 0; i < getNumFacilities(); i++)
		{
			fixedCosts += sol.sourceFacility.at_element(l, i)*e1FixedCosts.at_element(l, i);
		}
	}

	return varCosts + fixedCosts;
}

double lrp::Problem::e2shippingCosts(Solution & sol)
{
	double varCosts = 0.0;
	for (unsigned int i = 0; i < getNumFacilities(); i++)
	{
		// supplied demand of CDC i in I
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			for (unsigned int m = 0; m < getNumSources(); m++)
			{
				varCosts += customers.at(j).getDemand(m)*sol.facilityCustomer.at_element(i, j)*facilities.at(i).getVarCost();
			}
		}
	}

	double fixedCosts = 0.0;
	for (unsigned int i = 0; i < getNumFacilities(); i++)
	{
		for (unsigned int j = 0; j < getNumCustomers(); j++)
		{
			fixedCosts += sol.facilityCustomer.at_element(i, j)*e2FixedCosts.at_element(i, j);
		}
	}

	return varCosts + fixedCosts;
}

double lrp::Problem::operatingCosts(Solution & sol)
{
	return e1operatingCosts(sol)+e2operatingCosts(sol);
}

double lrp::Problem::COemissions(Solution & sol)
{
	return e1COemissions(sol) + e2COemissions(sol);
}

double lrp::Problem::CO2emissions(Solution & sol)
{
	return e1CO2emissions(sol) + e2CO2emissions(sol);
}

double lrp::Problem::shippingCosts(Solution & sol)
{
	return e1shippingCosts(sol) + e2shippingCosts(sol);
}
