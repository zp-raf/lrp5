#include "lrp.h"

lrp::Source::Source(std::string _name, double _varCost) : varCost(_varCost), Entity(_name)
{
	if (_varCost < 0)
		throw std::exception("Source properties must not be negative");
}

lrp::Customer::Customer(std::vector<double> _dem, std::string _name) : demand(_dem), Entity(_name)
{
	for (auto p : _dem)
	{
		if (p < 0)
			throw std::exception("Customer properties must be non negative");
	}
}

lrp::Facility::Facility(double _opCost, double _delCost, double _maxCap, std::string _name)
	: openingCost(_opCost), varCost(_delCost), maxCapacity(_maxCap), Entity(_name)
{
	if (_opCost < 0 || _delCost < 0 || _maxCap < 0)
		throw std::exception("Facility properties must be non-negative");
}

lrp::Vehicle::Vehicle(double _maxCap, double _maxDist, double _maxTime, double _costDist, std::string _name)
	: maxCapacity(_maxCap), maxTravelDistance(_maxDist), maxTravelTime(_maxTime), costPerDistance(_costDist), Entity(_name)
{
	if (_maxCap < 0 || _maxTime < 0 || _maxDist < 0 || _costDist < 0)
		throw std::exception("Vehicle parameters must be non-negative");
}

lrp::Problem::Problem(std::string dataFileName)
{
	fetchData(dataFileName); // load data from a JSON file
	name = dataFileName;

	// Evaluate feasibility of the problem
	double totalDemand = 0.0;
	for (auto c : customers)
	{
		for (unsigned int l = 0; l < getNumSources(); l++)
		{
			totalDemand += c.getDemand(l);
		}
	}
	if (getNumSources() > getE1MaxNumRoutes()) // if E1 fleet capacity is exceeded
		throw std::exception("Unfeasible problem: not enough vehicles at the first echelon");
	if (std::isgreater(totalDemand, getE2FleetCapacity())) // if E2 fleet capacity is exceeded
		throw std::exception("Unfeasible problem: customer demand exceeds total E2 fleet capacity");

	// Push all constraints into the constraints vector
	constraints.push_back(Constraint("First-echelon vehicle capacity constraint", std::bind(&Problem::e1vehCapacityConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("Second-echelon vehicle capacity constraint", std::bind(&Problem::e2vehCapacityConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("First-echelon route length constraint", std::bind(&Problem::e1rteLengthConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("Second-echelon route length constraint", std::bind(&Problem::e2rteLengthConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("First-echelon route duration constraint", std::bind(&Problem::e1rteDurationConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("Second-echelon route duration constraint", std::bind(&Problem::e2rteDurationConstraint, this, std::placeholders::_1)));
	constraints.push_back(Constraint("CDC capacity constraint", std::bind(&Problem::facCapacityConstraint, this, std::placeholders::_1)));

	// Push objective functions pointer into a vector
	objectiveFunctions.push_back(Objective("CDC establishing costs", std::bind(&Problem::openingCosts, this, std::placeholders::_1), mobjective::OptType::Min));
	//objectiveFunctions.push_back(Objective("First-echelon vehicle operating costs", std::bind(&Problem::e1operatingCosts, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("Second-echelon vehicle operating costs", std::bind(&Problem::e2operatingCosts, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("First-echelon CO emissios", std::bind(&Problem::e1COemissions, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("First-echelon CO2 emissions", std::bind(&Problem::e1CO2emissions, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("Second-echelon CO emissions", std::bind(&Problem::e2COemissions, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("Second-echelon CO2 emissions", std::bind(&Problem::e2CO2emissions, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("First-echelon shipping costs", std::bind(&Problem::e1shippingCosts, this, std::placeholders::_1), multi_obj::OptType::Min));
	//objectiveFunctions.push_back(Objective("Second-echelon shipping costs", std::bind(&Problem::e2shippingCosts, this, std::placeholders::_1), multi_obj::OptType::Min));

	objectiveFunctions.push_back(Objective("Total CO emissions", std::bind(&Problem::COemissions, this, std::placeholders::_1), mobjective::OptType::Min));
	objectiveFunctions.push_back(Objective("Total CO2 emissions", std::bind(&Problem::CO2emissions, this, std::placeholders::_1), mobjective::OptType::Min));
	objectiveFunctions.push_back(Objective("Total Vehicle operating costs", std::bind(&Problem::operatingCosts, this, std::placeholders::_1), mobjective::OptType::Min));
	objectiveFunctions.push_back(Objective("Total Shipping costs", std::bind(&Problem::shippingCosts, this, std::placeholders::_1), mobjective::OptType::Min));
}

bool lrp::Problem::evalFeasibilityOf(Solution & sol, bool silent)
{
	for (auto c : constraints)
	{
		//if (!(this->*c.runConstraint)(sol)) // if one constraint returns false
		if (!c.runConstraint(sol)) // if one constraint returns false
		{
			if (!silent)
				std::cout << "Constraint " << c.name << " not satisfied" << std::endl;
			return false;
		}
	}
	return true;
}

double lrp::Problem::getOverallConstViolation(Solution & sol)
{
	return e1vehCapacityViolation(sol) + e2vehCapacityViolation(sol)
		+ e1rteLengthViolation(sol) + e2rteLengthVioliation(sol)
		+ e1rteDurationVioliation(sol) + e2rteDurationVioliation(sol)
		+ facCapacityVioliation(sol);
}

double lrp::Problem::getE2FleetCapacity()
{
	double result = 0.0;
	for (auto e2 : e2Vehicles)
	{
		result += e2.getMaxCapacity();
	}
	return result;
}

double lrp::Problem::getFacilityDemand(unsigned int product, unsigned int facility, Solution & sol)
{
	if (product >= getNumSources())
		throw std::exception("Invalid source index");
	if (facility >= getNumFacilities())
		throw std::exception("Invalid facility index");

	double demand = 0.0;
	for (unsigned int j = 0; j < getNumCustomers(); j++)
	{
		demand += customers.at(j).getDemand(product)*sol.facilityCustomer.at_element(facility, j);
	}
	return demand;
}

double lrp::Problem::getFacilityDemand(unsigned int product, unsigned int facility, Solution::FacilityCustomerMatrix & facilityCustomer)
{
	if (product >= getNumSources())
		throw std::exception("Invalid source index");
	if (facility >= getNumFacilities())
		throw std::exception("Invalid facility index");

	double demand = 0.0;
	for (unsigned int j = 0; j < getNumCustomers(); j++)
	{
		demand += customers.at(j).getDemand(product)*facilityCustomer.at_element(facility, j);
	}
	return demand;
}

unsigned int lrp::Problem::getNumTrips(unsigned int manufacturer, unsigned int depot, Solution::FacilityCustomerMatrix & facCus, Problem & prob)
{
	return static_cast<unsigned int> (std::ceil(prob.getFacilityDemand(manufacturer, depot, facCus) / prob.getE1VehicleCapacity()));
}

mobjective::Objectives lrp::Problem::getObjectiveValuesFrom(Solution & sol)
{
	// check second level routing vector dimensionality (should be 3)
	if (sol.secondEchelonRoutes.num_dimensions() != 3)
		throw std::exception("Invalid routing vector dimensions number");

	// check second level routing vector dimension sizes
	typedef Solution::RouteArray array_t;
	unsigned int kMax = sol.secondEchelonRoutes.shape()[2];
	unsigned int iMax = sol.secondEchelonRoutes.shape()[0];
	unsigned int jMax = sol.secondEchelonRoutes.shape()[1];
	if (kMax != getE2NumVehicles() || iMax != (getNumCustomers() + getNumFacilities()) || jMax != (getNumCustomers() + getNumFacilities()))
		throw std::exception("Invalid routing vector sizes");

	// check first level routing vector dimensionality (should be 3)
	if (sol.firstEchelonRoutes.num_dimensions() != 3)
		throw std::exception("Invalid routing vector dimensions number");

	// check first level routing vector dimension sizes
	kMax = sol.firstEchelonRoutes.shape()[2];
	iMax = sol.firstEchelonRoutes.shape()[0];
	jMax = sol.firstEchelonRoutes.shape()[1];
	if (kMax != getE1MaxNumRoutes() || iMax != (getNumSources() + getNumFacilities()) || jMax != (getNumSources() + getNumFacilities()))
		throw std::exception("Invalid routing vector sizes");

	mobjective::Objectives result;

	for (auto o : objectiveFunctions)
	{
		mobjective::Objective temp;
		temp.name = o.name;
		temp.type = o.objectiveType;
		//temp.value = (this->*o.getObjectiveValue)(sol);
		temp.value = o.getObjectiveValue(sol);
		result.push_back(temp);
	}

	return result;
}

lrp::Solution::Solution(Problem& problem)
{
	facilityStatus = FacilityStatusVector(problem.getNumFacilities(), 0);
	facilityCustomer = FacilityCustomerMatrix(problem.getNumFacilities(), problem.getNumCustomers(), 0);
	sourceFacility = SourceFacilityMatrix(problem.getNumSources(), problem.getNumFacilities(), 0);
	Solution::RouteArray::extent_gen extents;
	secondEchelonRoutes.resize(extents[problem.getNumFacilities() + problem.getNumCustomers()][problem.getNumFacilities() + problem.getNumCustomers()][problem.getE2NumVehicles()]);
	firstEchelonRoutes.resize(extents[problem.getNumFacilities() + problem.getNumSources()][problem.getNumFacilities() + problem.getNumSources()][problem.getE1MaxNumRoutes()]);
	fractionOfDemand.resize(extents[problem.getNumSources()][problem.getNumFacilities()][problem.getE1MaxNumRoutes()]);
}