# About

This is an implementation of a Two-Echelon Green Location-Routing Problem with five objective functions. It is meant to be used with external solving techniques.

# Description

This project is an static library meant to be used with other projects to obtain objective values and feasibility from a given solution, abstracting the solving technique from the specific problem implementation.

# Dependencies

* Visual Studio 2015 or newer.
* boost libraries 1.63.00 or newer <http://www.boost.org>.

# Setup

1. Create an environmet variable `VSPROJDIR` containing the full path to the directory in which the repo will be cloned.
2. Clone the repo in `VSPROJDIR`.
3. Download in decompress boost 1.63.00 library in `C:\boost`.
4. Open project in Visual Studio and check _Additional Include Directories_ for `C:\boost\boost_1_63_0` libraries.
5. Check within Visual Studio that the project is compiled as an static library.