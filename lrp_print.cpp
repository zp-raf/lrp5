#include "lrp.h"
#include <sstream>

void lrp::printSolution(Solution & sol, lrp::Problem & prob)
{
	std::cout << sol2str(sol, prob) << std::endl;
}

void lrp::printProblem(lrp::Problem & prob)
{
	std::cout << data2str(prob);
}

//std::string lrp::sol2str(Solution sol, lrp::Problem & problem)
//{
//	std::ostringstream out;
//	//out << std::endl;
//	out << "****************SOLUTION DATA****************" << std::endl;
//
//	//out << "Routing 3D vector (a matrix for each vehicle)" << std::endl;
//	unsigned int kMax = sol.secondEchelonRoutes.shape()[2];
//	unsigned int iMax = sol.secondEchelonRoutes.shape()[0];
//	unsigned int jMax = sol.secondEchelonRoutes.shape()[1];
//
//	//for (unsigned int k = 0; k < kMax; k++)
//	//{
//	//	for (unsigned int i = 0; i < iMax; i++)
//	//	{
//	//		for (unsigned int j = 0; j < jMax; j++)
//	//		{
//	//			out << std::setw(2) << sol.routes[i][j][k] << ' ';
//	//		}
//	//		out << std::endl;
//	//	}
//	//	out << std::endl << std::endl;
//	//}
//	//
//	//out << "Facility status" << std::endl;
//	//for (auto fs : sol.facilityStatus)
//	//{
//	//	out << fs << " ";
//	//}
//	//
//	//out << std::endl << "Facility customer assignment" << std::endl;
//	//for (size_t i = 0; i < sol.facilityCustomer.size1(); i++)
//	//{
//	//	for (size_t j = 0; j < sol.facilityCustomer.size2(); j++)
//	//	{
//	//		out << sol.facilityCustomer.at_element(i, j) << " ";
//	//	}
//	//	out << std::endl;
//	//}
//	//
//	//unsigned int kMax = sol.routes.shape()[2];
//	//unsigned int iMax = sol.routes.shape()[0];
//	//unsigned int jMax = sol.routes.shape()[1];
//
//	/*
//		PRINT SECOND ECHELON ROUTES
//	*/
//	for (unsigned int k = 0; k < kMax; k++)
//	{
//		out << "Second Echelon Route for Vehicle " << k << ": ";
//		bool theEnd = false;
//		unsigned int i = 0;
//		while (i < iMax && !theEnd)
//		{
//			unsigned int j = 0;
//			while (j < jMax && !theEnd)
//			{
//				if (sol.secondEchelonRoutes[i][j][k] == 1 && i < problem.getNumFacilities()) // the very first node
//				{
//					out << "(" << i << ")->" << j - problem.getNumFacilities();
//					i = j;
//					j = 0;
//				}
//				else if (sol.secondEchelonRoutes[i][j][k] == 1 && j < problem.getNumFacilities()) // the last node
//				{
//					out << "->(" << j << ")";
//					theEnd = true;
//				}
//				else if (sol.secondEchelonRoutes[i][j][k] == 1) // every other node
//				{
//					out << "->" << j - problem.getNumFacilities();
//					i = j;
//					j = 0;
//				}
//				else
//				{
//					++j;
//				}
//			}
//			++i;
//		}
//		out << std::endl;
//	}
//
//	/*
//		PRINT FIRST ECHELON ROUTES
//	*/
//	for (unsigned int k = 0; k < kMax; k++)
//	{
//		out << "First Echelon Route no. " << k << ": ";
//		bool theEnd = false;
//		unsigned int i = 0;
//		while (i < iMax && !theEnd)
//		{
//			unsigned int j = 0;
//			while (j < jMax && !theEnd)
//			{
//				if (sol.firstEchelonRoutes[i][j][k] == 1 && i < problem.getNumSources()) // the very first node
//				{
//					out << "(" << i << ")->" << j - problem.getNumSources();
//					i = j;
//					j = 0;
//				}
//				else if (sol.firstEchelonRoutes[i][j][k] == 1 && j < problem.getNumSources()) // the last node
//				{
//					out << "->(" << j << ")";
//					theEnd = true;
//				}
//				else if (sol.firstEchelonRoutes[i][j][k] == 1) // every other node
//				{
//					out << "->" << j - problem.getNumSources();
//					i = j;
//					j = 0;
//				}
//				else
//				{
//					++j;
//				}
//			}
//			++i;
//		}
//		out << std::endl;
//	}
//
//	//for (size_t i = 0; i < sol.facilityCustomer.size1(); i++)
//	//{
//	//	out << "Facility " << i << " demand: ";
//	//	double demand = 0.0;
//	//	for (size_t j = 0; j < sol.facilityCustomer.size2(); j++)
//	//	{
//	//		if (sol.facilityCustomer.at_element(i, j) == 1)
//	//		{
//	//			demand += problem.getCustomerDemand(j);
//	//		}
//	//	}
//	//	out << demand << std::endl;
//	//}
//	//
//	//for (size_t i = 0; i < sol.sourceFacility.size1(); i++)
//	//{
//	//	for (size_t j = 0; j < sol.sourceFacility.size2(); j++)
//	//	{
//	//		if (sol.sourceFacility.at_element(i, j) != 0)
//	//		{
//	//			out << "Transport costs for facility " << j << ": " << sol.sourceFacility.at_element(i, j)*problem.supplyTransportCosts.at_element(i, j) << std::endl;
//	//		}
//	//	}
//	//}
//	//
//	//for (size_t i = 0; i < sol.sourceFacility.size1(); i++)
//	//{
//	//	for (size_t j = 0; j < sol.sourceFacility.size2(); j++)
//	//	{
//	//		if (sol.sourceFacility.at_element(i, j) != 0)
//	//		{
//	//			out << "Source for facility " << j << ": " << i << std::endl;
//	//			out << "Demanded quantity for facility " << i << ": " << sol.sourceFacility.at_element(i, j) << std::endl;
//	//		}
//	//	}
//	//}
//	//
//	//out << "****************OBJECTIVE FUNCTIONS****************" << std::endl;
//	//
//	//for (auto obj : sol.objectives)
//	//{
//	//	out << obj.name << ": " << obj.value << std::endl;
//	//}
//	for (auto obj : sol.objectives)
//	{
//		out << ";" << obj.name;
//	}
//	out << std::endl;
//	for (auto obj : sol.objectives)
//	{
//		out << ";" << obj.value;
//	}
//
//	return out.str();
//}

template <class T>
std::string lrp::vec2str(std::vector<T>& vec, std::string name)
{
	std::ostringstream out;
	out << "\n" << name << ":" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto &v : vec)
	{
		out << v << "\t";
	}
	out << std::endl;
	return out.str();
}

template <class T>
std::string lrp::mat2str(ublas::matrix<T> mat, std::string name)
{
	std::ostringstream out;
	out << "\n" << name << ":" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (unsigned int i = 0; i < mat.size1(); i++)
	{
		for (unsigned int j = 0; j < mat.size2(); j++)
		{
			out << mat.at_element(i, j) << "\t";
		}
		out << std::endl;
	}
	out << std::endl;
	return out.str();
}

template <class T>
std::string lrp::multiArray2str(boost::multi_array<T,3> arr, std::string name)
{
	std::ostringstream out;
	out << "\n" << name << ":" << std::endl;
	out << "-------------------------------------" << std::endl;
	unsigned int kMax = arr.shape()[2];
	unsigned int iMax = arr.shape()[0];
	unsigned int jMax = arr.shape()[1];
	for (unsigned int k = 0; k < kMax; k++)
	{
		for (unsigned int i = 0; i < iMax; i++)
		{
			for (unsigned int j = 0; j < jMax; j++)
			{
				out << arr[i][j][k] << "\t";
			}
			out << std::endl;
		}
		out << std::endl;
	}
	out << std::endl;
	return out.str();
}

std::string lrp::sol2str(Solution sol, lrp::Problem & problem)
{
	std::ostringstream out;
	out << "S O L U T I O N    D A T A" << std::endl;
	out << "------------------------" << std::endl;
	out << std::endl;

	out << multiArray2str(sol.firstEchelonRoutes, "First-echelon routes");
	out << multiArray2str(sol.secondEchelonRoutes, "Second-echelon routes");
	out << mat2str(sol.facilityCustomer, "Facility-Customer assignment");
	out << mat2str(sol.sourceFacility, "Source-Facility assignment");
	out << vec2str(sol.facilityStatus, "Facility status");
	out << multiArray2str(sol.fractionOfDemand, "Fraction of demand transported");

	out << "****************OBJECTIVE FUNCTIONS****************" << std::endl;
		
	for (auto obj : sol.objectives)
	{
		out << obj.name << ": " << obj.value << std::endl;
	}
	for (auto obj : sol.objectives)
	{
		out << ";" << obj.name;
	}
	out << std::endl;
	for (auto obj : sol.objectives)
	{
		out << ";" << obj.value;
	}

	return out.str();
}

std::string lrp::data2str(lrp::Problem& problem)
{
	std::ostringstream out;
	out << "P R O B L E M    D A T A" << std::endl;
	out << "------------------------" << std::endl;
	out << std::endl;
	out << "\nManufacturer data" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto m : problem.sources)
	{
		out << "Name: " << m.getName() << ", Variable Delivery Cost: " << m.getVarCost() << std::endl;
	}
	out << "\nCDC data" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto f : problem.facilities)
	{
		out << "Name: " << f.getName() << ", Variable Delivery Cost: " << f.getVarCost() << ", Max Capacity: " << f.getMaxCapacity() << ", Opening Cost: " << f.getOpeningCost() << std::endl;
	}
	out << "\nCustomer data" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto c : problem.customers)
	{
		out << "Name: " << c.getName();
		for (unsigned int p = 0; p < problem.getNumSources(); p++)
		{
			out << ", demand of product from " << problem.sources.at(p).getName() << ": " << c.getDemand(p);
		}
		out << std::endl;
	}
	out << "\nFirst-echelon vehicle fleet" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto v : problem.e1Vehicles)
	{
		out << "Name: " << v.getName() << ", max travel distance: " << v.getMaxTravelDistance() << ", max travel time: " << v.getMaxTravelTime() << ", max capacity: " << v.getMaxCapacity() << ", operating cost: " << v.getCostPerDistance() << std::endl;
	}
	out << "\nMaximum number of first-echelon routes: " << problem.e1MaxNumRoutes << std::endl;
	out << "-------------------------------------" << std::endl;
	out << "\nSecond-echelon vehicle fleet" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto v : problem.e2Vehicles)
	{
		out << "Name: " << v.getName() << ", max travel distance: " << v.getMaxTravelDistance() << ", max travel time: " << v.getMaxTravelTime() << ", max capacity: " << v.getMaxCapacity() << ", operating cost: " << v.getCostPerDistance() << std::endl;
	}
	out << "\nPollutants" << std::endl;
	out << "-------------------------------------" << std::endl;
	for (auto p : problem.pollutants)
	{
		out << "Name: " << p.getName() << std::endl;
	}
	out << mat2str(problem.e1EmissionFactors, "First-echelon emission factors");
	out << mat2str(problem.e2EmissionFactors, "Second-echelon emission factors");
	out << mat2str(problem.e1Distances, "First-echelon distances");
	out << mat2str(problem.e2Distances, "Second-echelon distances");
	out << multiArray2str(problem.e1TravelTimes, "First-echelon travel times");
	out << multiArray2str(problem.e2TravelTimes, "Second-echelon travel times");
	out << mat2str(problem.e1UnloadTimes, "First-echelon unload times");
	out << mat2str(problem.e2UnloadTimes, "Second-echelon unload times");
	out << mat2str(problem.e1FixedCosts, "First-echelon fixed delivery costs");
	out << mat2str(problem.e2FixedCosts, "Second-echelon fixed delivery costs");
	return out.str();
}
