#pragma once

#include "mobjective.h"
#include <boost/multi_array.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <functional>
#include <string>
#include <vector>

// The index of the vehicle's vector, that corresponds to the vehicle type of the first echelon
#define E1_VEH_IDX 0

// The carbon monoxide index of the pollutants vector
#define CO_IDX 0

// The carbon dioxide index of the pollutants vector
#define CO2_IDX 1

/*
Aliases for Boost namespaces
*/
namespace pt = boost::property_tree;
namespace ublas = boost::numeric::ublas;

namespace lrp {
	class Entity;
	class Source;
	class Facility;
	class Customer;
	class Vehicle;
	class Problem;

	// This is a base class for all other entities of the LRP problem
	class Entity
	{
	private:
		std::string name;
		double varCost;
	public:
		Entity(std::string _name) : name(_name) {};
		std::string getName() { return name; };
		double getVarCost() { return varCost; };
	};

	class Source : public Entity
	{
	private:
		double varCost;
	public:
		Source(std::string _name, double _varCost);
		double getVarCost() { return varCost; };
	};

	class Facility : public Entity
	{
	private:
		double openingCost;
		double varCost;
		double maxCapacity;
		//double envImpact;
	public:
		Facility(double _opCost, double _delCost, double _maxCap, std::string _name);
		double getOpeningCost() { return openingCost; };
		double getVarCost() { return varCost; };
		double getMaxCapacity() { return maxCapacity; };
		//double GetEnvImpact() { return envImpact; };
	};

	class Customer : public Entity
	{
	private:
		std::vector<double> demand;
	public:
		Customer(std::vector<double> _dem, std::string _name);
		double getDemand(unsigned int product) { return demand.at(product); };
	};

	class Vehicle : public Entity
	{
	private:
		double maxCapacity;
		double maxTravelDistance;
		double maxTravelTime;
		double costPerDistance;
	public:
		Vehicle(double _maxCap, double _maxDist, double _maxTime, double _costDist, std::string _name);
		double getMaxCapacity() { return maxCapacity; };
		double getMaxTravelDistance() { return maxTravelDistance; };
		double getMaxTravelTime() { return maxTravelTime; };
		double getCostPerDistance() { return costPerDistance; };
	};

	class Pollutant : public Entity
	{
	public:
		Pollutant(std::string _name) : Entity(_name) {};
	};

	// A solution to the LRP problem
	struct Solution : public mobjective::BaseSolution {
		typedef std::vector<unsigned int> FacilityStatusVector;
		typedef ublas::matrix<unsigned int> FacilityCustomerMatrix;
		typedef ublas::matrix<unsigned int> SourceFacilityMatrix;
		typedef boost::multi_array<unsigned int, 3> RouteArray;
		typedef boost::multi_array<double, 3> FractionDemand;

		Solution(Problem&);

		// The opened/closed status of each facility
		FacilityStatusVector facilityStatus;

		// An IxJ matrix that contains a 1 if a customer J is served from a facility I, and a 0 otherwise
		FacilityCustomerMatrix facilityCustomer;

		// An LxI matrix that contains a 1 if facility I is served from source L, and 0 otherwise
		SourceFacilityMatrix sourceFacility;

		// A three-dimensional array L=(L+I) x I=(L+I) x K, that contains a 1 if the node L precedes I on the route K
		RouteArray firstEchelonRoutes;

		// A three-dimensional array I=(I+J) x J=(I+J) x K, that contains a 1 if the node I precedes J on the route K
		RouteArray secondEchelonRoutes;

		// A vector containing the fraction of the demand of a CDC that a first-echelon vehicle carries
		FractionDemand fractionOfDemand;
	};

	// For testing purposes only, prints a given solution to the standard output
	void printSolution(Solution &, Problem &);

	// For testing purposes only, prints a given problem data
	void printProblem(Problem &);

	template <class T>
	std::string vec2str(std::vector<T>& vec, std::string name);

	template <class T>
	std::string mat2str(ublas::matrix<T> mat, std::string name);

	template <class T>
	std::string multiArray2str(boost::multi_array<T, 3> arr, std::string name);

	class Problem
	{
		friend std::string sol2str(Solution, Problem &);
		friend std::string data2str(Problem &);
	public:
		Problem(std::string dataFileName);

		// Returns the value of the objective function for a given solution
		mobjective::Objectives getObjectiveValuesFrom(Solution &);

		// Return wether a solution is feasible or not
		bool evalFeasibilityOf(Solution & sol, bool silent = true);

		// Calculates the overall constraint violation of a given solution
		double getOverallConstViolation(Solution & sol);

		std::string getName() { return name; };
		unsigned int getNumFacilities() { return facilities.size(); };
		unsigned int getNumCustomers() { return customers.size(); };
		unsigned int getNumSources() { return sources.size(); };
		unsigned int getE2NumVehicles() { return e2Vehicles.size(); }
		double getE2FleetCapacity();
		unsigned int getNumPollutants() { return pollutants.size(); };
		unsigned int getE1MaxNumRoutes() { return e1MaxNumRoutes; };
		unsigned int getE1NumVehicles() { return e1Vehicles.size(); };
		double getE1Distance(unsigned int i, unsigned int j) { return e1Distances.at_element(i, j); };
		double getE1TravelTime(unsigned int i, unsigned int j) { return e1TravelTimes[i][j][E1_VEH_IDX]; };
		double getFacilityDemand(unsigned int product, unsigned int facility, Solution & sol);
		double getFacilityDemand(unsigned int product, unsigned int facility, Solution::FacilityCustomerMatrix & facilityCustomer);
		double getE1VehicleCapacity() { return e1Vehicles.at(E1_VEH_IDX).getMaxCapacity(); };
		double getE1VehicleMaxRteLength() { return e1Vehicles.at(E1_VEH_IDX).getMaxTravelDistance(); };
		double getE1VehicleMaxTravelTime() { return e1Vehicles.at(E1_VEH_IDX).getMaxTravelTime(); };
		unsigned int getNumObjectives() { return objectiveFunctions.size(); };
		unsigned int getNumTrips(unsigned int manufacturer, unsigned int depot, Solution::FacilityCustomerMatrix & facCus, Problem & prob);
	private:
		void fetchData(std::string);

		/*
			Type definitions for problem data
		*/

		typedef ublas::matrix<double> DistancesMatrix;
		typedef ublas::matrix<double> DeliveryCostsMatrix;
		typedef ublas::matrix<double> UnloadTimesMatrix;
		typedef ublas::matrix<double> EmissionFactorMatrix;
		typedef boost::multi_array<double, 3> TravelTimesArray;
		typedef std::vector<Source> SourcesVector;
		typedef std::vector<Facility> FacilitiesVector;
		typedef std::vector<Customer> CustomersVector;
		typedef std::vector<Vehicle> VehiclesVector;
		typedef std::vector<Pollutant> PollutantsVector;

		// A structure to provide a name and common call method for problem constraints
		struct Constraint {
			//typedef bool (Problem::* const ConstraintCall)(Solution);
			typedef std::function<bool(Solution)> ConstraintCall;
			const std::string name;
			ConstraintCall runConstraint;
			Constraint(std::string _name, ConstraintCall _funcPtr) : name(_name), runConstraint(_funcPtr) {};
		};

		// A vector that holds pointers to constraint checking member functions
		std::vector<Constraint> constraints;

		// A structure to provide a name and common call method for objective functions
		struct Objective {
			//typedef double (Problem::* const ObjectiveCall)(Solution);
			typedef std::function<double(Solution)> ObjectiveCall;
			const std::string name;
			ObjectiveCall getObjectiveValue;
			mobjective::OptType objectiveType;
			Objective(std::string _name, ObjectiveCall _funcPtr, mobjective::OptType _opType) : name(_name), getObjectiveValue(_funcPtr), objectiveType(_opType) {};
		};

		// A vector that holds pointers to objective functions methods
		std::vector<Objective> objectiveFunctions;

		// the problem name
		std::string name;

		/*
			Entities
		*/

		SourcesVector sources;
		FacilitiesVector facilities;
		CustomersVector customers;
		VehiclesVector e2Vehicles;
		VehiclesVector e1Vehicles;
		PollutantsVector pollutants;

		/*
			Second echelon data
		*/

		EmissionFactorMatrix e2EmissionFactors;
		DistancesMatrix e2Distances;
		DeliveryCostsMatrix e2FixedCosts;
		TravelTimesArray e2TravelTimes;
		UnloadTimesMatrix e2UnloadTimes;

		/*
			First echelon data
		*/

		unsigned int e1MaxNumRoutes;
		DistancesMatrix e1Distances;
		TravelTimesArray e1TravelTimes;
		DeliveryCostsMatrix e1FixedCosts;
		UnloadTimesMatrix e1UnloadTimes;
		EmissionFactorMatrix e1EmissionFactors;

		/*
			Objective functions
		*/
		double e1operatingCosts(Solution &);
		double e2operatingCosts(Solution &);
		double e1COemissions(Solution &);
		double e1CO2emissions(Solution &);
		double e2COemissions(Solution &);
		double e2CO2emissions(Solution &);
		double e1shippingCosts(Solution &);
		double e2shippingCosts(Solution &);

		double openingCosts(Solution &);
		double operatingCosts(Solution &);
		double COemissions(Solution &);
		double CO2emissions(Solution &);
		double shippingCosts(Solution &);

		/*
			Problem constraints
		*/

		bool e1vehCapacityConstraint(Solution &);
		bool e2vehCapacityConstraint(Solution &);
		bool e1rteLengthConstraint(Solution &);
		bool e2rteLengthConstraint(Solution &);
		bool e1rteDurationConstraint(Solution &);
		bool e2rteDurationConstraint(Solution &);
		bool facCapacityConstraint(Solution &);

		/*
			Problem constraint violiation
		*/

		double e1vehCapacityViolation(Solution &);
		double e2vehCapacityViolation(Solution &);
		double e1rteLengthViolation(Solution &);
		double e2rteLengthVioliation(Solution &);
		double e1rteDurationVioliation(Solution &);
		double e2rteDurationVioliation(Solution &);
		double facCapacityVioliation(Solution &);
	};

	namespace loader {
		template<class T>
		void vectorLoader(const std::string key, const pt::ptree& treeRoot, std::vector<T>& data, std::function<T(pt::ptree)> creator);

		template<class T>
		void matrixLoader(const std::string key, const pt::ptree& treeRoot, unsigned int size_i, unsigned int size_j, ublas::matrix<T>& data, std::function<T(pt::ptree)> creator);

		// Loads a LRP problem solution from a given file. An existing problem must be supplied for reference
		Solution getLrpSolution(std::string filename, Problem & targetProblem);
	}
}